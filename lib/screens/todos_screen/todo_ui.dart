import 'package:flutter/material.dart';
import 'package:provider_demo/components/todo_card.dart';
import 'package:provider_demo/models/todo_item.dart';
import 'package:lottie/lottie.dart';

class TodoUI extends StatelessWidget {
  const TodoUI(
      {this.todos,
      this.textFieldController,
      this.onAddTodoButtonPressed,
      this.onDeleteButtonPressed,
      this.onUpdateStatusButtonPressed});

  final Function onAddTodoButtonPressed;
  final Function onDeleteButtonPressed;
  final Function onUpdateStatusButtonPressed;
  final TextEditingController textFieldController;
  final List<TodoItem> todos;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'My To-Do App',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: Container(
            child: ListView(
          children: <Widget>[
            Container(
              height: 300,
              width: 300,
              child: Lottie.asset('assets/lotties/todo.json'),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                          labelText: 'What do you want to do today?'),
                      controller: textFieldController,
                    ),
                  ),
                  FlatButton(
                    onPressed: onAddTodoButtonPressed,
                    child: Container(
                        height: 50,
                        width: 50,
                        child: Lottie.asset('assets/lotties/add.json')),
                  ),
                ],
              ),
            ),
            ...todos.map((TodoItem e) {
              return TodoItemCard(
                todoItem: e,
                onDeleteButtonPressed: onDeleteButtonPressed,
                onUpdateStatusButtonPressed: onUpdateStatusButtonPressed,
              );
            }),
          ],
        )),
      ),
    );
  }
}
